using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootPlane : MonoBehaviour
{
    public Transform OpenFanceTransform;
    public Material material;


    [ColorUsage(true, true)]
    public Color myColor;

    bool bCol = false;
    bool bColorUp = true;
    float fFanceAngle = 0;

    void Start()
    {
    }

    void Update()
    {
        if (bCol)
        {
            fFanceAngle -= 20 * Time.deltaTime;
            if (fFanceAngle >= -92.0)
            {
                OpenFanceTransform.rotation = Quaternion.Euler(0, 90, fFanceAngle);
            }
        }
        else
        {
            if (bColorUp)
            {
                myColor.r += 1.5f * Time.deltaTime;
                myColor.g += 1.5f * Time.deltaTime;
                myColor.b += 1.5f * Time.deltaTime;

                if(myColor.r >= 0.5f)
                {
                    bColorUp = false;
                }
            }
            else
            {
                myColor.r -= 1.5f * Time.deltaTime;
                myColor.g -= 1.5f * Time.deltaTime;
                myColor.b -= 1.5f * Time.deltaTime;

                if (myColor.r <= 0.0f)
                {
                    bColorUp = true;
                }
            }

            material.SetColor("_EmissionColor",myColor);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!bCol && collision.gameObject.CompareTag("Player"))
        {
            myColor.r = 0.0f;
            myColor.g = 0.0f;
            myColor.b = 0.0f;
            material.SetColor("_EmissionColor", myColor);

            //OpenFanceTransform.rotation = Quaternion.Euler(0, 90, -10);

            bCol = true;
        }


    }
}
