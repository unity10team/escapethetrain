using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMgr : MonoBehaviour
{
    public AudioClip[] clip;
    AudioSource AudioSource;
    public static SoundMgr instance;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }

        AudioSource = GetComponent<AudioSource>();
        AudioSource.clip = clip[0];
        AudioSource.Play();
    }

    public void SoundPlay(string soundName, AudioClip clip)
    {
        GameObject obj = new GameObject(soundName + "Sound");
        AudioSource audioSource = obj.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.Play();

        Destroy(obj, clip.length); //클립 길이만큼 재생후 삭제
    }

    public void PlayBossBGM(bool isPlay)
    {
        if (isPlay)
        {
            AudioSource.clip = clip[1];
            AudioSource.Play();
        }
        else
            AudioSource.Stop();

    }
}
