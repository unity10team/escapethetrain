using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionCan : MonoBehaviour
{
    public GameObject mashObj;
    public GameObject effectObj;
    public AudioClip clip;
    Follow cameraCs;

    int boomCnt = 0;
    bool isBoom = false;

    void Awake()
    {
        cameraCs = GameObject.Find("Main Camera").GetComponent<Follow>();
    }
    void Start()
    {
        boomCnt = 5;
    }

    void Update()
    {
        if(!isBoom && boomCnt <= 0)
        {
            cameraCs.SetShaking();
            isBoom = true;
            SoundMgr.instance.SoundPlay("spawn", clip);

            mashObj.SetActive(false);
            effectObj.SetActive(true);

            RaycastHit[] rayHits = Physics.SphereCastAll(
                transform.position, 150, Vector3.up,
                0f, LayerMask.GetMask("Boss"));

            foreach (RaycastHit hitObj in rayHits)
            {
                hitObj.transform.GetComponent<SkeletonBoss>().HitByGrenade();
            }

            Invoke("DeleteCan", 2.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        bool isHit = false;
        if (other.tag == "Melee")
        {
            isHit = true;
        }
        else if (other.tag == "Bullet")
        {
            isHit = true;
            Destroy(other.gameObject);
        }

        if (isHit)
            boomCnt -= 1;
    }

    void DeleteCan()
    {
        Destroy(gameObject);
    }
}
