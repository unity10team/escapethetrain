using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    public GameObject boss;
    Transform transformPlayer;

    bool isSpawn = false;
    // Update is called once per frame
    void Awake()
    {
        transformPlayer = GameObject.Find("Player").transform;

    }
    void Update()
    {
        
        if (!isSpawn && transformPlayer.position.z >= 130.0f)
        {
            isSpawn = true;
            Instantiate(boss, new Vector3(0.0f, 3.65f, 165.0f), transform.rotation);

            SoundMgr.instance.PlayBossBGM(true);
        }
    }
}
