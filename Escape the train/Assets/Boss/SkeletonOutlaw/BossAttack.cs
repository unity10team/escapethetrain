using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttack : MonoBehaviour
{
    public enum Type
    {
        ThrowAxe,
        EffectShot
    };
    public Type type;
    public bool isLeftHand = false;
    public Transform transformPlayer;

    float activeTime = 0.0f;

    void Awake()
    {
        //GetComponent<Transform>
        if (type == Type.ThrowAxe)
        {
            activeTime = 3.0f;
        }
        else if (type == Type.EffectShot)
        {
            activeTime = 1.4f;
            //transform.Rotate(new Vector3(0, 1, 0) * -90.0f);
            //transform.LookAt(transformPlayer);
        }
    }

    void Update()
    {
        activeTime -= Time.deltaTime;
        if (activeTime <= 0.0f)
            Destroy(gameObject);

        if (type == Type.ThrowAxe)
        {
            float rotSpped = 1000.0f;
            if (!isLeftHand)
                rotSpped *= -1.0f;

            transform.Rotate(new Vector3(0, 1, 0) * Time.deltaTime * rotSpped);
        }
        else if(type == Type.EffectShot)
        {

        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (type == Type.ThrowAxe)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Destroy(gameObject);
                collision.gameObject.GetComponent<Player>().SetHp(-10);
                //gameObject.SetActive(false);
            }
        }

    }
}
