using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SkeletonBoss : MonoBehaviour
{
    Transform transformPlayer;
    Follow cameraCs;

    public GameObject throwAxe;
    public GameObject effectShot;
    public Transform transformLHand;
    public Transform transformRHand;
    public Transform transformHead;
    public BoxCollider boxColLAxe;
    public BoxCollider boxColRAxe;
    public Light redLight;
    public Material material;

    public AudioClip[] clip; //스폰, 스윙01, 스윙2, 웃음, 레이저, 피격, 주금


    //public float invokValue = 0.4f;
    NavMeshAgent nav;
    Animator anim;
    Rigidbody rigid;
    //Material material;

    enum STATE { IDLE, WALK, ATTACK, THROW, SHOT, DEAD};
    STATE eState = STATE.IDLE;

    float attackRange = 5.0f;
    float idleTime = 0.0f;
    float walkToThrowTime = 0.0f;
    float throwTime = 0.0f;

    bool isCameraShake = false;
    bool isRightThorw = false;
    bool isRun = false;
    bool isShot = false;
    bool isCinematic = false;
    bool isLowHp = false;
    bool isDead = false;
    bool isLightRange = false;

    string strWalkOrRun = "isWalk";

    int attackIndex = 0;
    public int curHp = 0, maxHp = 0;

    void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        rigid = GetComponent<Rigidbody>();

        transformPlayer = GameObject.Find("Player").transform;
        cameraCs = GameObject.Find("Main Camera").GetComponent<Follow>();
        GameObject.Find("Game Manager").GetComponent<GameManager>().boss = this;

        //material = transform.Find("Boots").GetComponent<MeshRenderer>().material;

        //transformLHand = transform.Find("ORG-hand_L");
        //transformRHand = transform.Find("ORG-hand_R");
        //redLight = GetComponentInChildren<Light>();
        //cameraCs = camera.GetComponent<Follow>();

    }
    void Start()
    {
        SoundMgr.instance.SoundPlay("Boss", clip[0]);

        isRun = true;
        strWalkOrRun = "isWalk";/*"isRun";*/

        maxHp = 1500;
        curHp = maxHp;

        SetState(STATE.IDLE);

        StartCoroutine("SetCinematic", false);
    }

    void Update()
    {
        if (isLowHp && !isDead)
        {
            if (isLightRange)
                redLight.range += Time.deltaTime * 10.0f;
            else
            {
                if (redLight.range >= 1.0f) 
                    redLight.range -= Time.deltaTime * 10.0f;
            }

        }


        Vector3 targetPos = new Vector3(transformPlayer.position.x, transform.position.y, transformPlayer.position.z );
        if(!isDead)
            transform.LookAt(targetPos);


        float fDistance = Vector3.Distance(transform.position, transformPlayer.position);
        switch (eState)
        {
            case STATE.IDLE:
                {
                    idleTime -= Time.deltaTime;

                    if (idleTime <= 0.0f/* && fDistance <= 20.0f*/)
                        SetState(STATE.WALK);

                    break;
                }
            case STATE.WALK:
                {
                    //if (isCurAnimName("Walk00"))
                    nav.SetDestination(transformPlayer.position);

                    if (fDistance <= attackRange)
                        SetState(STATE.ATTACK);
                    else
                    {
                        walkToThrowTime -= Time.deltaTime;
                        if (walkToThrowTime <= 0.0f && fDistance <= 20.0f)
                        {
                            //shot은 거리
                            if (!isLowHp)
                                SetState(STATE.THROW);
                            else
                            {
                                SetState(STATE.SHOT);
                                //int n = Random.RandomRange(0, 1);
                                //if (n == 0)
                                //    SetState(STATE.THROW);
                                //else
                                //    SetState(STATE.SHOT);
                            }

                        }

                    }

                    break;
                }
            case STATE.ATTACK:
                {
                    if (attackIndex == 0 && isCurAnimName("Attack00") ||
                        attackIndex == 1 && isCurAnimName("Attack01") ||
                        attackIndex == 2 && isCurAnimName("Attack02"))
                    {
                        StartSwingCoroutine();
                    }

                    if (!isCameraShake && isCurAnimName("Attack02") &&
                            anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.45)
                    {
                        cameraCs.SetShaking();
                        isCameraShake = true;

                        if(isCinematic)
                            Invoke("isCameraShakeOff", 1.2f);
                        else
                            Invoke("isCameraShakeOff", 0.6f);
                    }

                    if (!isCinematic)
                    {
                        if (fDistance > attackRange * 2.0f)
                        {
                            SetState(STATE.IDLE);
                        }
                    }


                    break;
                }
            case STATE.THROW:
                {
                    //has exit time ����
                    throwTime -= Time.deltaTime;
                    if (throwTime <= 0.0f)
                    {
                        anim.SetBool("isThrow", false);
                        SetState(STATE.IDLE);
                        break;

                    }

                    if (!isRightThorw && isCurAnimName("Throw0"))
                    {
                        isRightThorw = true;
                        Invoke("MakeThrowAxe", 0.4f);
                    }
                    else if (isRightThorw && isCurAnimName("Throw1"))
                    {
                        isRightThorw = false;
                        Invoke("MakeThrowAxe", 0.4f);
                    }

                    break;
                }
            case STATE.SHOT:
                {
                    throwTime -= Time.deltaTime;

                    float fRatio = GetAnimProgressRatio();
                    if (fRatio <= 0.1f)
                        anim.speed = 0.1f;

                    if (fRatio >= 0.25f)
                        anim.speed = 1.0f;

                    //print(fRatio);
                    if(!isShot && fRatio >= 0.5f && fRatio < 1.0f)
                    {
                        isShot = true;
                        MakeEffectShot();
                        Invoke("isShotOff", 0.5f);

                        if (throwTime <= 0.0f)
                        {
                            anim.SetBool("isShot", false);
                            SetState(STATE.IDLE);
                            break;
                        }
                    }

                    //if (!isRightThorw && isCurAnimName("Shot0"))
                    //{
                    //    isRightThorw = true;
                    //    Invoke("MakeEffectShot", 2.0f);
                    //}
                    //else if (isRightThorw && isCurAnimName("Shot1"))
                    //{
                    //    isRightThorw = false;
                    //    Invoke("MakeEffectShot", 2.1f);
                    //}

                    break;
                }
            case STATE.DEAD:
                if (GetAnimProgressRatio() <= 1.0f && GetAnimProgressRatio() >= 0.9f)
                    anim.speed = 0;
                break;
        }

    }

    void SetState(STATE state)
    {
        //anim.speed = 1.0f;
        eState = state;
        switch (eState)
        {
            case STATE.IDLE:
                anim.SetBool(strWalkOrRun, false);
                anim.SetBool("isAttack", false);
                //anim.SetBool("isThrow", false);

                nav.isStopped = true;

                idleTime = Random.Range(0.5f, 2.0f);
                break;
            case STATE.WALK:
                if (isRun)
                    anim.speed = 1.3f;

                anim.SetBool(strWalkOrRun, true);
                anim.SetBool("isAttack", false);
               // anim.SetBool("isThrow", false);
                nav.isStopped = false;

                walkToThrowTime = Random.Range(2.0f, 4.0f);

                break;
            case STATE.ATTACK:
                anim.SetBool(strWalkOrRun, false);
                anim.SetBool("isAttack", true);
                anim.SetTrigger("doAttack");
                nav.isStopped = true;
                if (isCinematic)
                    anim.speed = 0.7f;

                    //anim.speed = 2.0f;
                    break;

            case STATE.THROW:
                anim.speed = 1.0f;
                anim.SetBool(strWalkOrRun, false);
                anim.SetBool("isThrow", true);
                anim.SetTrigger("doThrow");
                nav.isStopped = true;

                throwTime = Random.Range(2.0f, 4.0f); 
                isRightThorw = false;
                break;

            case STATE.SHOT:
                anim.speed = 0.1f;
                anim.SetBool(strWalkOrRun, false);
                anim.SetBool("isShot", true);
                anim.SetTrigger("doShot");
                nav.isStopped = true;

                throwTime = 4.0f; //2번 정도?
                isRightThorw = false;
                isShot = false;
                break;
            case STATE.DEAD:
                anim.speed = 0.2f;

                //anim.SetBool(strWalkOrRun, false);
                //anim.SetBool("isAttack", false);
                anim.SetTrigger("doDead");

                nav.isStopped = true;
                break;

        }
    }

    void StartSwingCoroutine()
    {
        StopCoroutine("Swing");
        StartCoroutine("Swing");
        attackIndex++;
        if (attackIndex == 3)
            attackIndex = 0;
    }

    IEnumerator Swing()
    {
        // 1
        yield return new WaitForSeconds(0.2f);  // 0.1s Waiting
        boxColLAxe.enabled = true;
        boxColRAxe.enabled = true;

        if(attackIndex == 0)
            SoundMgr.instance.SoundPlay("Boss", clip[2]);
        else
            SoundMgr.instance.SoundPlay("Boss", clip[1]);


        // 2
        yield return new WaitForSeconds(0.4f);  
        SetAxeColOff();
    }

    public void SetAxeColOff()
    {
        boxColLAxe.enabled = false;
        boxColRAxe.enabled = false;
    }

    IEnumerator SetCinematic(bool isEnding)
    {
        if (!isEnding)
        {
            idleTime = 5.0f;

            yield return new WaitForSeconds(1.0f);
            cameraCs.bossCs = this;
            cameraCs.SetBossCinematic(true);
            isCinematic = true;
            anim.SetBool("isCinematic", true);

            SetState(STATE.ATTACK);

            yield return new WaitForSeconds(5.0f);
            isCinematic = false;
            cameraCs.SetBossCinematic(false);

            anim.SetBool("isCinematic", false);
        }
        else
        {
            yield return new WaitForSeconds(0.5f);
            cameraCs.bossCs = this;
            cameraCs.SetBossCinematic(true);
            isCinematic = true;

            //yield return new WaitForSeconds(5.0f);
            //cameraCs.SetShaking();

            yield return new WaitForSeconds(7.0f);
            isCinematic = false;
            cameraCs.SetBossCinematic(false);
        }
    }

    void MakeThrowAxe()
    {
        SoundMgr.instance.SoundPlay("Boss", clip[1]);

        Vector3 pos;
        if (isRightThorw)
            pos = transformRHand.position;
        else
            pos = transformLHand.position;

        GameObject axe = Instantiate(throwAxe, pos, transform.rotation);
        Rigidbody bulletRigid = axe.GetComponent<Rigidbody>();
        bulletRigid.velocity = transform.forward * 15.0f;

        BossAttack bossAttackCs = axe.GetComponent<BossAttack>();
        if (isRightThorw)
            bossAttackCs.isLeftHand = true;

    }

    void MakeEffectShot()
    {
        SoundMgr.instance.SoundPlay("Boss", clip[4]);

        GameObject shot = Instantiate(effectShot, redLight.transform.position, transform.rotation);

        BossAttack bossAttackCs = shot.GetComponent<BossAttack>();
        bossAttackCs.transformPlayer = transformPlayer;
    }

    void isShotOff()
    {
        isShot = false;
    }

    void isCameraShakeOff()
    {
        isCameraShake = false;
    }

    void isLightRangeFix()
    {
        isLightRange = !isLightRange;
        Invoke("isLightRangeFix", 0.7f);
    }

    void FreezeVelocity()
    {
        //print(rigid.velocity);
        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
    }

    void FixedUpdate()
    {
        FreezeVelocity();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.tag);
        bool isHit = false;
        if (other.tag == "Melee") 
        {
            Weapon weapoon = other.GetComponent<Weapon>();
            curHp -= weapoon.damage;
            isHit = true;

        }
        else if (other.tag == "Bullet")
        {
            Bullet bullet = other.GetComponent<Bullet>();
            curHp -= bullet.damage;
            isHit = true;

            Destroy(other.gameObject);
        }

        if(isHit)
        {
            //dead
            if (curHp <= 0 )
            {
                if (!isDead)
                {
                    SoundMgr.instance.SoundPlay("Boss", clip[6]);
                    SoundMgr.instance.PlayBossBGM(false);
                    curHp = 0;//ui위해서
                    isDead = true;
                    SetState(STATE.DEAD);
                    StartCoroutine("SetCinematic", true);
                }
            }
            //onDamage
            else
            {
                Debug.Log("Boss Hp:" + curHp);
                StartCoroutine(OnDamage());
            }

            if (!isLowHp && curHp <= maxHp * 0.5f)
            {
                SoundMgr.instance.SoundPlay("Boss", clip[3]);

                nav.speed *= 1.5f;
                strWalkOrRun = "isRun";
                isLowHp = true;
                Invoke("isLightRangeFix", 0.7f);
            }
        }

    }

    IEnumerator OnDamage()
    {
        material.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        material.color = Color.white;
    }

    public void HitByGrenade()
    {
        //print("hit explo");
        SoundMgr.instance.SoundPlay("Boss", clip[5]);
        curHp -= 100;
        OnDamage();
    }

    bool IsEndAnimation(string animName)
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName(animName) &&
            anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.99;

    }

    bool isCurAnimName(string animName)
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName(animName);
    }

    float GetAnimProgressRatio()
    {
        return anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
}

