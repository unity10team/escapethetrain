﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AN_DoorKey : MonoBehaviour
{
    [Tooltip("True - red key object, false - blue key")]
    public bool isRedKey = true;

    public GameObject target;
    public AN_DoorScript DoorObject;

    // NearView()
    float distance;
    float angleView;
    Vector3 direction;

    private void Start()
    {
    }

    void Update()
    {
        if ( NearView() && Input.GetKeyDown(KeyCode.F) && target.GetComponent<Player>().hasKey)
        {
            print("열쇠열기");
            DoorObject.Action();   
        }
    }

    bool NearView() // it is true if you near interactive object
    {
        // 플레이어할
        distance = Vector3.Distance(DoorObject.GetComponent<Transform>().position, target.transform.position);
        if (distance < 2f) return true;
        else return false;
    }
}
