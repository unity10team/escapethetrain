using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float speed;
    public GameObject[] weapons;
    public bool[] hasWeapons;
    public Camera followCamera;
    public int hasGrenades;
    public GameObject grenadeObj;
    
    public int coin;
    public int hp;
    public int bullet;
    private int MaxHp;
    public int Att;
    public Button buyKey;
    public Text BombNum;
    
    public int JumpPower;
    //private int MaxBullet;
    public int MaxBullet;
    private float hAxis;
    private float vAxis;
    
    private bool jDown;
    private bool fDown;
    private bool gDown;
    private bool iDown;
    private bool sDown1;
    private bool sDown2;
    private bool sDown3;
    private bool dDown;
    
    private bool sDown4;
    private bool sDown5;
    private bool sDown6;
    
    private bool isJump;
    private bool isSwap;
    private bool isLand;
    private bool isFireReady = true;
    private bool isAttack;
    private bool isDodge;
    private bool isGrenade;
    private bool isShoot;
    public bool isShop;

    public int hpPotion;
    public int AttPotion;
    public int speedPotion;

    public bool hasKey;

    private Vector3 moveVec;

    private Rigidbody rigid;
    private Animator anim;

    private GameObject nearObject;  // 근처에 있는 아이템

    //Weapon equipWeapon;       //전
    public Weapon equipWeapon;  // 후
    // 포션 개수 저장할 변수 추가
    public int potionCnt;

    private int equipWeaponIndex = -1;

    private float fireDelay;

    private Vector3 grenadeNextVec;
    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        MaxBullet = 200;
        MaxHp = 600;
        hp = 400;

        // 최고 점수 저장
        Debug.Log(PlayerPrefs.GetInt("MaxScore"));
        //PlayerPrefs.SetInt("MaxScore", 112500);
    }

    private void Update()
    {
        GetInput();
        Move();
        Turn();
        Jump();
        Grenade();
        Attack();
        Swap();
        Interaction();
        Dodge();
        FreezeRotation();
        ItemEat();
        BombNum.text = hasGrenades.ToString();
        // z가 15 이상이면 몬스터 움직임 시작
    }

   
    void GetInput() 
    {
        hAxis = Input.GetAxisRaw("Horizontal");
        vAxis = Input.GetAxisRaw("Vertical");
        jDown = Input.GetButtonDown("Jump");
        fDown = Input.GetButton("Fire1");
        gDown = Input.GetButtonDown("Fire2");
        iDown = Input.GetButtonDown("Interaction");
        sDown1 = Input.GetButtonDown("Swap1");
        sDown2 = Input.GetButtonDown("Swap2");
        sDown3 = Input.GetButtonDown("Swap3");

        sDown4 = Input.GetButtonDown("Item4");
        sDown5 = Input.GetButtonDown("Item5");
        sDown6 = Input.GetButtonDown("Item6");
        
        dDown = Input.GetButtonDown("Dodge");
    }

    void ItemEat()
    {
        if (sDown4)
        {
            if (speedPotion > 0)
            {
                speed += 1;
                if (speed > 10)
                    speed = 10;
                speedPotion--;
            }
        }
        if (sDown5)
        {
            if (AttPotion > 0)
            {
                Att += 1;
                if (Att > 10)
                    Att = 10;
                AttPotion--;
            }
            
        }
        if (sDown6)
        {
            if (hpPotion > 0)
            {
                hp += 50;
                if (hp > MaxHp)
                    hp = MaxHp;
                hpPotion--;
            }
            
        }
    }
    void Move()
    {
        moveVec = Vector3.zero;
        if (isAttack || isLand || isShop)
            return;
        
        moveVec = new Vector3(hAxis, 0, vAxis).normalized;
        // if (isJump)     // 점프 중에는 반만 이동하게
        //     moveVec /= 2;
        if (isDodge)
        {
            // 회피중에도 이동하게 앞으로
            transform.position += transform.forward * Time.deltaTime * speed * 1.5f;
            return;
        }
        transform.position += moveVec * Time.deltaTime * speed;
        anim.SetBool("isRun", moveVec != Vector3.zero);
    }

    void Dodge()
    {
        //anim.get
        if (dDown && !isJump && !isDodge && !isShop)
        {
            anim.SetTrigger("doRoll");
            isDodge = true;
            //GetComponent<BreakableWindow>().breakWindow();
            Invoke("DodgeOut", 0.9f);
        }
    }
    
    void DodgeOut()
    {
        
        isDodge = false;
    }
    void Turn()
    {
        if (isDodge || isGrenade || isAttack)
            return;
        // 1. 키보드에 의한 회전
        transform.LookAt(transform.position + moveVec);

        // 2. 마우스에 의한 회전
        // if (fDown)
        // {
        //     Ray ray = followCamera.ScreenPointToRay(Input.mousePosition);
        //     RaycastHit rayHit;
        //     if (Physics.Raycast(ray, out rayHit, 100))
        //     {
        //         Vector3 nextVec = rayHit.point - transform.position;
        //         nextVec.y = 0;
        //         transform.LookAt(transform.position + nextVec);
        //     }
        // }

    }

    void Jump()
    {
        if (jDown && !isJump && !isSwap && !isLand && !isAttack && !isDodge && !isShop)
        {
            rigid.AddForce(Vector3.up * JumpPower, ForceMode.Impulse);
            anim.SetBool("isJump", true);
            anim.SetTrigger("doJump");
            isJump = true;
        }
    }

    void Attack()
    {
        if (equipWeapon == null)
            return;
        fireDelay += Time.deltaTime;
        isFireReady = equipWeapon.rate < fireDelay;


        if (fDown && isFireReady && !isLand && !isSwap && !isAttack && !isJump && !isShop)
        {
            equipWeapon.Use();
            fireDelay = 0;
            
            if (equipWeapon.type == Weapon.Type.Melee)
            {
                isAttack = true;
                anim.SetTrigger("doMelee");
                Invoke("AttackOut", 0.8f);
            }
            else
            {
                isShoot = true;
                anim.SetTrigger("doShoot");
                Invoke("ShootOut", 0.4f);
            }

        }
    }
    void AttackOut()
    {
        isAttack = false;
    }
    void ShootOut()
    {
        isShoot = false;
    }

    void SwapOut()
    {
        isSwap = false;
    }
    void Swap()
    {
        // 해당 무기가 없거나 해당 무기를 이미 장착중이면 실행 안함
        if (sDown1 && (!hasWeapons[0] || equipWeaponIndex == 0))
            return;
        if (sDown2 && (!hasWeapons[1] || equipWeaponIndex == 1))
            return;
        if (sDown3 && (!hasWeapons[2] || equipWeaponIndex == 2))
            return;
        
        int weaponIndex = -1;
        if (sDown1)
            weaponIndex = 0;
        if (sDown2)
            weaponIndex = 1;
        if (sDown3)
            weaponIndex = 2;
        
        // 점프 중에는 무기 체인지 불가
        if ((sDown1 || sDown2 || sDown3) && !isJump && !isLand && !isSwap && !isShop)
        {
            if (equipWeapon != null)
                equipWeapon.gameObject.SetActive(false);
            
            equipWeapon = weapons[weaponIndex].GetComponent<Weapon>();
            equipWeaponIndex = weaponIndex;
            equipWeapon.gameObject.SetActive(true);
            //equipWeapon.damage += Att;
            
            anim.SetTrigger("doSwap");

            isSwap = true;
            
            Invoke("SwapOut", 0.6f);
        }
    }
    
    void Interaction()
    {
        if (iDown && nearObject != null && !isJump && !isLand)
        {
            if (nearObject.tag == "Weapon")
            {
                Item item = nearObject.GetComponent<Item>();
                int weaponIndex = item.value;
                hasWeapons[weaponIndex] = true;

                Destroy(nearObject);
            }
            else if (nearObject.tag == "Item")
            {
                Item item = nearObject.GetComponent<Item>();
                Debug.Log(item.type);
                if (item.type == Item.Type.Bullet)
                {
                    bullet += item.value;
                    if (bullet > MaxBullet)
                        bullet = MaxBullet;
                }
                else if (item.type == Item.Type.Coin)
                {
                    coin += item.value;
                }
                else if (item.type == Item.Type.HpPotion)
                {
                    ++potionCnt;
                    hp += item.value;
                    if (hp > MaxHp)
                        hp = MaxHp;
                }

                Destroy(nearObject);
            }

            // 플레이어 피격(by 몬스터)
            else if (nearObject.tag == "Monster" || nearObject.tag == "MBullet")
            {
                hp -= 10;
            }
            else if (nearObject.tag == "Shop")
            {
                Shop shop = nearObject.GetComponent<Shop>();
                shop.Enter(this);
                isShop = true;
                
            }
        }
    }
    
    void FreezeRotation()
    {
        rigid.angularVelocity = Vector3.zero;
    }
    private void FixedUpdate()
    {
        
    }

    void Grenade()
    {
        if (hasGrenades == 0)
            return;
        if (gDown && !isSwap && !isGrenade)
        {
            Ray ray = followCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayHit;
            if (Physics.Raycast(ray, out rayHit, 100))
            {
                grenadeNextVec = rayHit.point - transform.position;
                grenadeNextVec.y = 8;
            }
            anim.SetTrigger("doGrenade");
            isGrenade = true;
            Invoke("GrenadeOut", 0.4f);


        }
    }

    void GrenadeOut()
    {
        GameObject instantGrenade = Instantiate(grenadeObj,
            transform.position, transform.rotation);
        Rigidbody rigidGrenade = instantGrenade.GetComponent<Rigidbody>();
        rigidGrenade.AddForce(grenadeNextVec,ForceMode.Impulse);
        rigidGrenade.AddTorque(Vector3.back*10, ForceMode.Impulse);
        isGrenade = false;
        hasGrenades--;
    }
    void LandOut()
    {
        isLand = false;
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Floor")
        {
            anim.SetBool("isJump",false);
            isJump = false;
            isLand = true;
            Invoke("LandOut", 0.4f);
        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Weapon" || other.tag == "Item" || other.tag == "Shop")
            nearObject = other.gameObject;
        
        // print(nearObject.name);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "BossAxe")
        {
            SetHp(-10);
            other.enabled = false;
        }
     
        if (other.tag == "Monster" || other.tag == "MBullet")
        {
            print("피격");
            hp -= 10;
        }
        // print(nearObject.name);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Weapon" || other.tag == "Item")
            nearObject = null;
        else if (other.tag == "Shop")
        {
            if (nearObject == null)
                return;
            
            Shop shop = nearObject.GetComponent<Shop>();
            shop.Exit();
            nearObject = null;
            isShop = false;

        }
    }

    public void SetHp(int damage)
    {
        hp += damage;
        //힐
        if (damage > 0)
        {
            if (hp >= MaxHp)
            {
                hp = MaxHp;
            }
        }
        //딜
        else
        {
            if (hp < 0)
                hp = 0;
        }
    }
}
