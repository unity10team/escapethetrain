using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    private void Awake()
    {
        _rigid = GetComponent<Rigidbody>();
        _boxCollider = GetComponent<BoxCollider>();
    }

    private void Update()
    {
        transform.Rotate(Vector3.up * 20 * Time.deltaTime);
    }

    public enum Type
    {
        // 아이템 종류 여기에 열거
        Weapon, HpPotion, Bullet, Coin
    };

    private Rigidbody _rigid;
    private BoxCollider _boxCollider;
    public Type type;
    public int value;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Floor")
        {
            _rigid.isKinematic = true;
            _boxCollider.enabled = false;
        }
    }
    
}
