using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage;
    private float startTime;
    private void Awake()
    {
        startTime = 0;
    }

    private void Update()
    {
        startTime += Time.deltaTime;
        if (startTime > 4)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        // GameObject item = null;
        // Vector3 vPos;
        if (other.gameObject.tag == "Floor" || other.gameObject.tag == "Box")
        {
            Destroy(gameObject);
        }
        // if (other.gameObject.tag == "coin_box")
        // {
        //     item = GameObject.FindWithTag("coin");
        //     vPos = other.transform.position;
        //     vPos.y -= 1.5f;
        //     Instantiate(item, vPos, Quaternion.identity);
        //     Destroy(other.gameObject);
        // }
        // if (other.gameObject.tag == "random_box")
        // {
        //     int randNum = UnityEngine.Random.Range(0, 2);
        //     if (randNum == 0)
        //         item = GameObject.Find("Bottle_Mana");
        //     else if (randNum == 1)
        //         item = GameObject.Find("Bottle_Health");
        //     else if (randNum == 2)
        //         item = GameObject.Find("Bottle_Endurance");
        //
        //     vPos = other.transform.position;
        //     vPos.y -= 1.0f;
        //     //Instantiate(item, vPos, Quaternion.identity);
        //     //Destroy(other.gameObject);
        // }
    }
}
