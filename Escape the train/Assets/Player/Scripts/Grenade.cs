using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    public GameObject mashObj;
    public GameObject effectObj;
    public Rigidbody rigid;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Explosion());
    }

    IEnumerator Explosion()
    {
        yield return new WaitForSeconds(3f);
        
        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
        mashObj.SetActive(false);
        effectObj.SetActive(true);

        // 몬스터 피격 처리
        // https://www.youtube.com/watch?v=FyJYWRIq0Ss&list=PLO-mt5Iu5TeYkrBzWKuTCl6IUm_bA6BKy&index=10&ab_channel=%EA%B3%A8%EB%93%9C%EB%A9%94%ED%83%88%EA%B3%A8%EB%93%9C%EB%A9%94%ED%83%88

        // RaycastHit[] rayHits = Physics.SphereCastAll(
        //     transform.position, 15, Vector3.up,
        //     5,0f,
        //     LayerMask.GetMask("Enemy"));
        //
        // foreach (RaycastHit hitObj in rayHits)
        // {
        //     hitObj.transform.GetComponent<Enemy>().HitByGrenade(transform.position);
        //     
        // }

        RaycastHit[] rayHits = Physics.SphereCastAll(
            transform.position, 15, Vector3.up,
            0f, LayerMask.GetMask("Boss"));

        foreach (RaycastHit hitObj in rayHits)
        {
            hitObj.transform.GetComponent<SkeletonBoss>().HitByGrenade();
            hitObj.transform.GetComponent<Enemy>().HitByGrenade();
        }
    }
}
