using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BangBox : MonoBehaviour
{
    public GameObject Particle;
    public GameObject OriginBox;
    public GameObject DestroyBox;
    public Item[] Items;
    public int value;
    
    private Animator anim;
    private BoxCollider boxCollider;
    private bool Destroyed = false;
    // Start is called before the first frame update
    private void Awake()
    {
        anim = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider>();
    }


    private void OnCollisionEnter(Collision other)
    {
        if ((other.gameObject.tag == "Melee" || other.gameObject.tag == "Bullet") && !Destroyed)
        {
            print("Bang");
            Destroyed = true;
            Particle.SetActive(false);
            OriginBox.SetActive(false);
            DestroyBox.SetActive(true);
            anim.SetBool("isBang", true);
            boxCollider.isTrigger = true;
            Instantiate(Items[value], transform.position, Quaternion.identity);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Melee" && !Destroyed)
        {
            print("Bang");
            Destroyed = true;
            Particle.SetActive(false);
            OriginBox.SetActive(false);
            DestroyBox.SetActive(true);
            anim.SetBool("isBang", true);
            boxCollider.isTrigger = true;

            Instantiate(Items[value], transform.position, Quaternion.identity);

        }
    }
}
