using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform target;

	public SkeletonBoss bossCs;

	public Vector3 offset;
	public bool isBossCinematic = false;

	bool doShake = false;
	float shakeTime = 0.0f;

    void Start()
    {
		isBossCinematic = false;
	}

    void Update()
    {
		if(!isBossCinematic)
			transform.position = target.position + offset;
		else
        {
			transform.position = bossCs.transformHead.position + (bossCs.transform.forward * 5.0f);
			transform.LookAt(bossCs.transformHead);
		}


		if (doShake)
		{
			shakeTime += Time.deltaTime * 5.0f;
			float Value = Mathf.Sin(shakeTime * 10.0f) * Mathf.Pow(0.50f, shakeTime);
			//Value *= 1.2f;
			transform.position += new Vector3(0.0f, Value, 0.0f);

			if (shakeTime > 2.0f)
			{
				shakeTime = 0.0f;
				doShake = false;
			}
		}
	}

	public void SetShaking()
    {
		doShake = true;
		shakeTime = 0.0f;
	}

	public void SetBossCinematic(bool _isCinema)
    {
		isBossCinematic = _isCinema;
		if (isBossCinematic)
        {
			//
        }
        else
        {
			transform.rotation = Quaternion.Euler(40, 0, 0);
        }

	}

}
