using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceMgr : MonoBehaviour
{
    public GameObject dice1;
    public GameObject dice2;
    public GameObject dice3;

    private bool isBroken;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isBroken)
            return;
        
        if (dice1.GetComponent<Dice>().NowNum == 2 &&
            dice2.GetComponent<Dice>().NowNum == 3 &&
            dice3.GetComponent<Dice>().NowNum == 1)
        {
            isBroken = true;
            Invoke("BrokeWindow", 0.6f);
        }
    }

    void BrokeWindow()
    {
        this.GetComponent<BreakableWindow>().breakWindow();

    }
}



