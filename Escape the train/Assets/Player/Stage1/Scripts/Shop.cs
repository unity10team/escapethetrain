using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public RectTransform uiGroup;
    public int[] itemPrice;

    public Text talkText;
    
    private Player enterPlayer;
    
    
    public void Enter(Player player)
    {
        enterPlayer = player;
        uiGroup.anchoredPosition = Vector2.zero;
        
    }

    public void Exit()
    {
        uiGroup.anchoredPosition = Vector2.down * 1000;
        if (enterPlayer != null)
            enterPlayer.isShop = false;
    }

    public void Buy(int index)
    {
        
        int price = itemPrice[index];
        if(enterPlayer == null)
            return;
        
        if (price > enterPlayer.coin)
        {
            StopCoroutine(Talk());
            StartCoroutine(Talk());
            return;
        }

        enterPlayer.coin -= price;
        // 아이템에 해당하는 플레이어 게이지 상승
        switch (price)
        {
            // 물약, 체력 공격력 이동속도
            case 500:
                enterPlayer.hpPotion += 1;
                break;
            case 1000:
                enterPlayer.AttPotion += 1;
                break;
            case 4000:
                enterPlayer.speedPotion += 1;
                break;
            case 2000:  // 소모품 수류탄 열쇠 총알
                enterPlayer.hasGrenades += 1;
                break;
            case 3000:
                enterPlayer.hasKey = true;
                enterPlayer.buyKey.interactable = false;
                
                break;
            case 1500:
                enterPlayer.bullet += 10;
                break;
        }
    }

    IEnumerator Talk()
    {
        talkText.text = "돈이 부족합니다.";
        yield return new WaitForSeconds(2f);

        talkText.text = "";
        
    }
}
