using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
    private bool isRoll;
    public int NowNum;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(isRoll)
            transform.Rotate(Vector3.up);

    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Melee" && !isRoll)
        {
            isRoll = true;
            print("타격");
            Invoke("RollOut", 0.6f);
        }
    }

    void RollOut()
    {
        switch (NowNum)
        {
            case 1:
                NowNum = 2;
                // 회전값 변경
                transform.rotation = Quaternion.Euler(0,90,90);
                break;
            case 2:
                NowNum = 3;
                transform.rotation = Quaternion.Euler(90, 90, 180);
                break;
            case 3:
                NowNum = 1;
                transform.rotation = Quaternion.Euler(-180,0,180);
                break;
            default:
                break;
        }

        isRoll = false;
    }
}
