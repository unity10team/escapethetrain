using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject menuCam;
    public GameObject gameCam;
    public Player player;
    public SkeletonBoss boss;
    public int stage;
    public float playTime;
    public bool isBattle;   // ���� �ο�� �ִ°�

    // ���������� �����ִ� ���� ��
    public int nFirstMCnt;
    public int nAfterMCnt;
    public int chargeMCnt;
    public int farMCnt;

    // UI
    public GameObject menuPanel;

    public GameObject gamePanel;
    public Text stageTxt;
    public Text playTimeTxt;
    // �÷��̾� ���� ����
    public Text playerHealthTxt;
    public Text playerAmmoTxt;
    public Text playerCoinTxt;
    // ����
    public Image weapon1Img;
    public Image weapon2Img;
    public Image weapon3Img;
    public Image weaponRImg;
    // ���� ��
    public Text nFirstMTxt;
    public Text nAfterMTxt;
    public Text chargeMTxt;
    public Text farMTxt;
    // ������ ����
    public Text playerPotionTxt;
    public Text speedPotionTxt;
    public Text attackPotionTxt;

    // ���� hp�� ���� ������������ ���;� ��
    public GameObject bossHealthGroup;
    // ���� ü�¿� ���� ������ ����
    public RectTransform bossHealthBar;

    bool stage1 = true;
    bool stage2 = false;
    bool stage3 = false;
    bool stage4 = false;

    bool isBossSpawn = false;
    public void GameStart()
    {
        // �޴� ���� ������Ʈ - ��Ȱ��ȭ
        // ���� ���� ������Ʈ - Ȱ��ȭ
        menuCam.SetActive(false);
        gameCam.SetActive(true);

        menuPanel.SetActive(false);
        gamePanel.SetActive(true);

        player.gameObject.SetActive(true);

        bossHealthGroup.SetActive(false);
    }

    void Update()
    {
        playTime += Time.deltaTime;

        if(boss && !isBossSpawn)
        {
            isBossSpawn = true;
            bossHealthGroup.SetActive(true);
        }

    }

    // Update() �� ȣ���
    void LateUpdate()
    {


        if (player.transform.position.z > 125f)
        {
            stage4 = true; stage1 = false; stage3 = false; stage2 = false;
        }
        else if (player.transform.position.z > 95f)
        {
            stage3 = true; stage1 = false; stage2 = false; stage4 = false;
        }
        else if (player.transform.position.z > 43f)
        {
            stage2 = true; stage1 = false; stage3 = false; stage4 = false;
        }
        else
        {
            stage1 = true; stage2 = false; stage3 = false; stage4 = false;
        }
        //scoreTxt.text = string.Format("{0:n0}", player.score);

        if (stage1)
            stageTxt.text = "STAGE " + 1;
        else if (stage2)
            stageTxt.text = "STAGE " + 2;
        else if (stage3)
            stageTxt.text = "STAGE " + 3;
        else if (stage4)
            stageTxt.text = "STAGE " + 4;


        int hour = (int)(playTime / 3600);
        int min = (int)((playTime - hour * 3600) / 60);
        int second = (int)(playTime % 60);
        playTimeTxt.text = string.Format("{0:00}", min) + " : " + string.Format("{0:00}", second);

        playerHealthTxt.text = player.hp + " / 600";
        playerCoinTxt.text = string.Format("{0:n0}", player.coin);
        // ������
        playerPotionTxt.text = string.Format("{0:n0}", player.hpPotion + player.potionCnt);
        attackPotionTxt.text = string.Format("{0:n0}", player.AttPotion);
        speedPotionTxt.text = string.Format("{0:n0}", player.speedPotion);

        //if (player.equipWeapon == null)
        //    playerAmmoTxt.text = " - / " + player.MaxBullet;
        //else if (player.equipWeapon.type == Weapon.Type.Melee)
        //    playerAmmoTxt.text = " - / " + player.MaxBullet;
        //else
        if (player.bullet > 200)
            player.bullet = 200;
        playerAmmoTxt.text = player.bullet + " / " + player.MaxBullet;

        if (player.hasWeapons[0])
            weapon1Img.color = new Color(0, 0, 0, 1);
        else
            weapon1Img.color = new Color(0, 0, 0, 0);
        if (player.hasWeapons[1])
            weapon2Img.color = new Color(0, 0, 0, 1);
        else
            weapon2Img.color = new Color(0, 0, 0, 0);
        if (player.hasWeapons[2])
            weapon3Img.color = new Color(0, 0, 0, 1);
        else
            weapon3Img.color = new Color(0, 0, 0, 0);
        if (player.hasGrenades > 0)
            weaponRImg.color = new Color(0, 0, 0, 1);
        else
            weaponRImg.color = new Color(0, 0, 0, 0);

        // ���� ��
        nFirstMTxt.text = nFirstMCnt.ToString();
        nAfterMTxt.text = nAfterMCnt.ToString();
        chargeMTxt.text = chargeMCnt.ToString();
        farMTxt.text = farMCnt.ToString();

        // ���� ü�¹�
        if (boss != null)
            bossHealthBar.localScale = new Vector3((float)boss.curHp / boss.maxHp, 1, 1);
    }
}
