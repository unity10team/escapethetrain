using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

public class AfterAtt : MonoBehaviour
{
    enum STATE { STATE_IDLE, STATE_RUN, STATE_ATTACK, STATE_END };

    NavMeshAgent navi;
    Animator ani;
    public int hp;
    public Transform ChaseObj;
    public float speed;

    private STATE eState;

    // 플레이어 공격용 충돌체
    public BoxCollider meleeArea;

    // Start is called before the first frame update
    void Start()
    {
        navi = GetComponent<NavMeshAgent>();
        ani = GetComponentInChildren<Animator>();
        ani.SetBool("isRun", false);
        eState = STATE.STATE_IDLE;
    }

    // Update is called once per frame
    void Update()
    {
        // 플레이어가 허들 넘고 나서 움직임
        if (ChaseObj.position.z < 15)
            return;

        Change_State();
        Change_Ani();

    }

    void Change_State()
    {
        if (eState == STATE.STATE_IDLE)
        {
            if (Vector3.Distance(transform.position, ChaseObj.position) <= 6.0f)
                eState = STATE.STATE_RUN;
        }
        else
        {
            if (Vector3.Distance(transform.position, ChaseObj.position) <= 3.0f)
                eState = STATE.STATE_ATTACK;
            else // STATE_RUN
                navi.SetDestination(ChaseObj.position);
            //anim.SetBool("isAttack", false);
        }
    }

    void Change_Ani()
    {
        if (eState == STATE.STATE_IDLE)
            ani.SetBool("isRun", false);
        else if (eState == STATE.STATE_RUN)
            ani.SetBool("isRun", true);
        else if (eState == STATE.STATE_ATTACK)
            ani.SetBool("isAttack", true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Melee")
        {
            
        }
    }
}
