using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    // ���� ����
    public enum TYPE {NORAML_FIRST, NORMAL_AFTER, CHARGE, FAR, TYPE_END};
    public TYPE eEnemyType;

    // ü��
    public int maxHealth;
    public int curHealth;
    public int score;

    public bool isChase;
    public bool isAttack;
    public bool isDead;
    // �÷��̾�� ���ݴ��ߴ���
    private bool isHit = false;
    bool isStart = false;
    bool bStart = false;
    private bool attStart = false;


    public Transform target;
    public GameObject bullet;
    public GameObject[] coins;
    public TrailRenderer trailEffect;
    public GameObject startEff;
    public AudioSource attSound;
    float time;

    // ������
    public GameObject[] enemies;
    // ������������ ��� ���Ͱ� �󸶳� ��ȯ�ɰ���
    public List<int> enemyList;

    Rigidbody rigid;
    BoxCollider boxColl;
    NavMeshAgent nav;
    Animator anim;

    private void Awake()
    {
        // enemyList�� �� ������������ ��Ÿ�� ���͵��� �����͸� �����ؾ� ��
        //enemyList = new List<int>();

        //GameObject player = GameObject.Find("Player");
        //target = player.transform;
        rigid = GetComponent<Rigidbody>();
        boxColl = GetComponent<BoxCollider>();
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();

        // 2�� �ڿ� ���� ����
        if (eEnemyType != TYPE.NORMAL_AFTER)
            Invoke("ChaseStart", 2);
    }


    // �÷��̾��� ����, �Ѿ�
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Melee" || other.tag == "Bullet")   // ����
        {
            if (eEnemyType == TYPE.NORMAL_AFTER)
            {
                Debug.Log("chasestart");
                isHit = true;
                Invoke("ChaseStart", 2);
            }
            
            if(other.tag == "Melee") 
                curHealth -= other.GetComponent<Weapon>().damage;
            else
            {
                curHealth -= other.GetComponent<Bullet>().damage;
            }

            // �˹� ó��
            Vector3 reactVec = transform.position - other.transform.position;

            StartCoroutine(OnDamage(reactVec));

            // �Ѿ� : Destroy(other.gameObject);
        }
        else if (other.tag == "Player")
        {

        }
    }

    IEnumerator OnDamage(Vector3 reactVec)
    {

        // 0.1�� ������
        yield return new WaitForSeconds(0.1f);
        if (curHealth > 0)
        {
            // �ǰ�
            curHealth -= 50;

            // �˹�
            reactVec = reactVec.normalized;
            reactVec += Vector3.up;
            rigid.AddForce(reactVec * 5, ForceMode.Impulse);
        }
        else
        {
            // Enemy Dead Layer�� ����
            gameObject.layer = 7;

            // ����
            isDead = true;
            isChase = false;
            //anim.SetTrigger("doDie");
            nav.enabled = false;

            int ranCoin = Random.Range(0, 5);
            for (int i = 0; i < ranCoin; ++i)
            {
                Vector3 vPos = transform.position;
                int ran = Random.Range(1, 4);
                if (ran == 1)
                    vPos.x += i * 1.2f;
                else if (ran == 2)
                    vPos.x -= i * 1.2f;
                else if (ran == 3)
                    vPos.z += i * 1.2f;
                else
                    vPos.z -= i * 1.2f;
                Instantiate(coins[0], vPos, Quaternion.identity);
            }
            Destroy(gameObject, 2);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    IEnumerator StartEff()
    {
        Instantiate(startEff, transform.position, transform.rotation);
        yield return new WaitForSeconds(2f);
        Destroy(startEff);
    }

    void CheckStart()
    {
        if (!bStart)
        {
            if (eEnemyType == TYPE.NORAML_FIRST)
            {
                if (target.position.z >= 100f)
                {
                    bStart = true;
                    //Instantiate(startEff, transform.position, transform.rotation);
                }
            }
            else if (eEnemyType == TYPE.CHARGE)
            {
                if (target.position.z >= 110f)
                {
                    bStart = true;
                    //Instantiate(startEff, transform.position, transform.rotation);

                }
            }
            else if (eEnemyType == TYPE.FAR)
            {
                if (target.position.z >= 120f)
                {
                    bStart = true;
                    //Instantiate(startEff, transform.position, transform.rotation);
                }
            }
        }
        else
        {
            //StartCoroutine("StartEff");
            isChase = true;
            anim.SetBool("isRun", true);
            //DestroyImmediate(startEff);
            CancelInvoke("CheckStart");
            return;
        }
    }
    public void HitByGrenade()
    {
        print("hit explo");
        //SoundMgr.instance.SoundPlay("Boss", clip[5]);
        curHealth -= 100;
        StartCoroutine(OnDamage(Vector3.back));
    }
    void ChaseStart()
    {
        if (!bStart)
        {
            InvokeRepeating("CheckStart", 0.1f, 0.1f);

        }
        if (bStart)
        {
            //StartCoroutine("StartEff");
            //isChase = true;
            //anim.SetBool("isRun", true);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (eEnemyType != TYPE.NORMAL_AFTER)
        {
            if (nav.enabled)
            {
                nav.SetDestination(target.position);
                nav.isStopped = !isChase;
            }
        }
        else if (eEnemyType == TYPE.NORMAL_AFTER)
        {
            if (nav.enabled && isHit)
            {
                nav.SetDestination(target.position);
                nav.isStopped = !isChase;
            }
        }

    }


    void FreezeVelocity()
    {
        if (isChase)
        {
            rigid.velocity = Vector3.zero;
            rigid.angularVelocity = Vector3.zero;
        }
    }

    void Targeting()
    {
        float targetRadius = 1.5f;
        float targetRange = 3f;

        switch (eEnemyType)
        {
            case TYPE.NORAML_FIRST:
                targetRadius = 1.5f;
                targetRange = 3f;
                break;
            case TYPE.CHARGE:
                targetRadius = 1f;
                targetRange = 12f;
                break;
            case TYPE.FAR:
                targetRadius = 0.5f;
                targetRange = 25f;
                break;
        }

        RaycastHit[] rayHits =
            Physics.SphereCastAll(transform.position,
            targetRadius,
            transform.forward,
            targetRange,
            LayerMask.GetMask("Player"));

        float fDis = Vector3.Distance(target.position, transform.position);
        // ���� �������� �ƴϰ� ���� �� ���� �ȿ� �÷��̾ ����
        if (/*rayHits.Length > 0 &&*/ !isAttack)
        {
            if (eEnemyType == TYPE.NORAML_FIRST && target.position.z >= 100f)
                attStart = true;
            else if (eEnemyType == TYPE.CHARGE && target.position.z >= 110f)
                attStart = true;
            else if (eEnemyType == TYPE.FAR && target.position.z >= 120f)
                attStart = true;
            else if (eEnemyType == TYPE.NORMAL_AFTER && isHit && fDis <= 1f)
                attStart = true;

            if (attStart)
            {
                if (eEnemyType == TYPE.NORAML_FIRST && fDis < 3f)
                    StartCoroutine(Attack());
                else if (eEnemyType == TYPE.NORMAL_AFTER && fDis < 3f)
                    StartCoroutine(Attack());
                else if (eEnemyType == TYPE.CHARGE && fDis < 7f)
                    StartCoroutine(Attack());
                else if (eEnemyType == TYPE.FAR && fDis < 15f)
                    StartCoroutine(Attack());
            }
        }
    }

    IEnumerator Attack()
    {
        isChase = false;
        isAttack = true;
        anim.SetBool("isAttack", true);
        Vector3 vDir = target.position - transform.position;
        vDir = vDir.normalized;
        transform.LookAt(target);

        switch (eEnemyType)
        {
            case TYPE.NORAML_FIRST:
                // 0.2�� �ڿ� ����(���ݹ��� Ȱ��ȭ)
                yield return new WaitForSeconds(0.2f);
                attSound.Play();

                // -> 2�� �ڿ� ���� ��
                yield return new WaitForSeconds(1f);

                anim.SetBool("isAttack", false);
                break;

            case TYPE.NORMAL_AFTER:
                attSound.Play();
                // -> 2�� �ڿ� ���� ��
                yield return new WaitForSeconds(1f);

                //anim.SetBool("isAttack", false);
                break;

            case TYPE.CHARGE:
                // 0.1�� �� ������
                yield return new WaitForSeconds(0.1f);
                // ����
                attSound.Play();
                StopCoroutine("Charge");
                StartCoroutine("Charge");

                rigid.AddForce(vDir * 20, ForceMode.Impulse);
                //meleeArea.enabled = true;
                // ���� ���� ���� �����
                yield return new WaitForSeconds(0.5f);
                rigid.velocity = Vector3.zero;
                yield return new WaitForSeconds(1f);


                // ���� �� ��Ȱ��ȭ
                //meleeArea.enabled = false;
                anim.SetBool("isAttack", false);
                // ��� ���� �� 2�� �޽�
                yield return new WaitForSeconds(3f);
                break;

            case TYPE.FAR:
                Debug.Log("far start");
                // �Ѿ� ��� ���� �غ�
                yield return new WaitForSeconds(0.3f);

                // �Ѿ� �����
                GameObject instantBullet = Instantiate(bullet, transform.position, transform.rotation);
                //attSound.Play();
                // �Ѿ� ���
                Rigidbody rigidBullet = instantBullet.GetComponent<Rigidbody>();
                rigidBullet.velocity = vDir * 20;

                //anim.SetBool("isAttack", false);
                yield return new WaitForSeconds(3f);
                DestroyImmediate(instantBullet);
                break;
        }


        // �ٽ� �Ѿư�
        isChase = true;
        isAttack = false;
        anim.SetBool("isAttack", false);
    }    

    IEnumerator Charge()
    {
        //yield return new WaitForSeconds(0.1f);
        trailEffect.enabled = true;

        yield return new WaitForSeconds(2f);
        trailEffect.enabled = false;
    }



    void FixedUpdate()
    {
        Targeting();
        FreezeVelocity() ;
    }
}
