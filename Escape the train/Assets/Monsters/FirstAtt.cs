using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

public class FirstAtt : MonoBehaviour
{
    // ���߿� �׺�޽� �߰��ϸ� �� �� ���
    NavMeshAgent nav;
    Animator anim;

    public Transform target;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        anim.SetBool("isWalk", true);

    }

    // Update is called once per frame
    void Update()
    {
        // 플레이어가 허들 넘고 나서 움직임
        if (target.position.z < 15)
            return;
        
        if (Vector3.Distance(transform.position, target.position) <= 3.0f)
        {
            anim.SetBool("isAttack", true);
        }
        else
        {
            nav.SetDestination(target.position);
            anim.SetBool("isAttack", false);
        }
    }
}
